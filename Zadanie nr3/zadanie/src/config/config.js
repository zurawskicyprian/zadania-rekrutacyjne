import { getPrice } from '../common/NBP';

export const currencies = [{
    name: 'USD',
    getPrice: getPrice('usd'),
},
{
    name: 'CHF',
    getPrice: getPrice('chf'),
},
{
    name: 'EUR',
    getPrice: getPrice('eur'),
},
]