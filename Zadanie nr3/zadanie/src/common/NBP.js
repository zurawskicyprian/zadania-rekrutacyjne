const baseApiUrl = 'https://api.nbp.pl/api/exchangerates/rates/a/'
export const getPrice = (currency) => {
	return () => {
    	return fetch(`${baseApiUrl}${currency}`)
        .then(res => res.json())
        .then(res => {
            if(!res['rates'] || !res['rates'][0] || !res['rates'][0]['mid']){
                return null
            }
            return res['rates'][0]['mid']
        })	
        
	}
}