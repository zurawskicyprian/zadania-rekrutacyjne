import React, { useState, useEffect } from 'react';
import { PRICE_REFRESH_INTERVAL } from '../../config/consts'

const Currency = (props) => {
  const [price, setPrice] = useState(null);
  const [loading, setLoading] = useState(false);

  const getPrice = () => {
	if(loading) return;
	setLoading(true);
    props.getPrice().then(price => {
        setPrice(price);
        setLoading(false);
    })
  }
  useEffect(() => {
    const interval = setInterval(() => {
    	getPrice();
    }, PRICE_REFRESH_INTERVAL);
    getPrice();
    return () => clearInterval(interval);
  }, []);
  return (
    <div className="price">
    	{!price ? '' : `${props.name}: `}{price}
    </div>
  );
};
export default Currency;
