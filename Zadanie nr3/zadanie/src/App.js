import React from 'react'
import Currency from './components/currency/Currency'
import {currencies} from './config/config'

function App() {
  return (
    <div className="App">
      {currencies.map((currency, index) =><Currency {...currency} key={index} />)}
    </div>
  );
}

export default App;
