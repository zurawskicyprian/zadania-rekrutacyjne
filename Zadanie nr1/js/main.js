const showSquareBtn = document.querySelector('.showSquare');
const fibbonacciArray = [0, 1, 1];
const N = 20;
let fibonacciDiv;

function generateFibbonacci(){
    for(let i = 3; i < N; i++){
        fibbonacciArray.push(fibbonacciArray[i - 1] + fibbonacciArray[i - 2]);
    }
};

function generateSquare(){
    for(let i = 0; i < N; i++){
        fibonacciDiv = document.createElement('div');
        document.querySelector('.wrapper').appendChild(fibonacciDiv).style.cssText = styleSquare(fibbonacciArray[i]);
    }
};

function styleSquare(width){
    return  `width:${width}px; height:${width}px; display:inline-block;`
};

showSquareBtn.addEventListener('click', function(){
    generateSquare();
    showSquareBtn.style.display = 'none';
});

generateFibbonacci();
