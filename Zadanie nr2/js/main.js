const readImage = new FileReader();

document.querySelector('.inputImage').addEventListener('change', function(e){
    readImage.onload = function(){
        console.log(e.target.files)
        localStorage.setItem('image',readImage.result) 
        document.querySelector('.img').setAttribute('src', localStorage.getItem('image') );
        document.querySelector('.img').onload=getExif;

    }
    readImage.readAsDataURL(e.target.files[0])
})

function getExif() {
    EXIF.getData(document.querySelector('.img'), function() {
        let allMetaData = EXIF.getAllTags(this);
        getLatLng(allMetaData)
    });
}
function getLatLng(allMetaData){
    let lat = (allMetaData.GPSLatitude[0]) + (allMetaData.GPSLatitude[1]/60) + (allMetaData.GPSLatitude[2]/3600);
    let lng = (allMetaData.GPSLongitude[0].numerator / allMetaData.GPSLongitude[0].denominator) + ((allMetaData.GPSLongitude[1].numerator / allMetaData.GPSLongitude[1].denominator)/60) + ((allMetaData.GPSLongitude[2].numerator / allMetaData.GPSLongitude[2].denominator)/3600);
    var map = L.map('map').setView([lat, lng], 15);
    L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=cSUnQGG6Anjzdwb6uHxs',{
      tileSize: 512,
      zoomOffset: -1,
      minZoom: 1,
      attribution: "\u003ca href=\"https://www.maptiler.com/copyright/\" target=\"_blank\"\u003e\u0026copy; MapTiler\u003c/a\u003e \u003ca href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\"\u003e\u0026copy; OpenStreetMap contributors\u003c/a\u003e",
      crossOrigin: true
    }).addTo(map);
    L.marker([lat, lng]).addTo(map)
}